<apex:page standardController="Contact" extensions="CtrlUportal360" docType="html-5.0" showHeader="false" sidebar="false">
    <html lang="en">
        <style>
            .lightning-icon-container {
                display: flex;
                align-items: center;
            }

            .lightning-icon {
                height: 1.5em;
            }

            .lightning-font {
                font-size: 1em;
            }

            .lightning-button-primary {
                background: #0070d2 !important;
                color: #ffffff !important;
                font-weight: 500 !important;
                padding: 6px !important;
                border-radius: 8px !important;
                font-size: 12px !important;
                box-shadow: 2px 1px #696969 !important;
            }

            .lightning-button-success {
                background: #10dc60 !important;
                color: #ffffff !important;
                font-weight: 500 !important;
                padding: 6px !important;
                border-radius: 8px !important;
                font-size: 12px !important;
                box-shadow: 2px 1px #696969 !important;
            }

            .lightning-table {
                border-collapse: collapse;
                margin: 0 0 16px;
                min-width: 400px;
                width: 100%;
                box-shadow: 0 3px 6px #00000029, 0 3px 6px #0000003b;
            }

            .lightning-table thead tr {
                background-color: #f8f4f4;
                color: #717170;
                text-align: left;
            }

            .lightning-table th,
            .lightning-table td {
                padding: 12px 15px;
            }

            .lightning-table td {
                font-size: 0.9em;
            }

            .lightning-table tbody tr {
                border-bottom: 1px solid #dddddd;
            }

            .lightning-table tbody tr:nth-of-type(even) {
                background-color: #f3f3f3;
            }

            .lightning-table tbody tr:last-of-type {
                border-bottom: 2px solid #999999;
            }
        </style>

        <script type="text/javascript">
            const openOnNewTab = (url) => {
                if (url === null || url.trim() === '') {
                    alert('URL link is empty!');
                    return;
                }
                window.open(url);
            };
        </script>

        <body>
            <apex:outputPanel id="mainBody" layout="block" style="padding: 4px">
                <apex:pageMessage
                    rendered="{!uiPage.uiMainPageMessage != null}"
                    strength="{!uiPage.uiMainPageMessage.strength}"
                    title="{!uiPage.uiMainPageMessage.title}"
                    summary="{!uiPage.uiMainPageMessage.message}"
                    severity="{!uiPage.uiMainPageMessage.severity}"
                ></apex:pageMessage>
                <apex:pageMessage
                    rendered="{!uiPage.uiMainPageMessage != null}"
                    strength="1"
                    summary="Refresh the page to try again. If this error persists, please contact salesforcedev@ugafinance.com"
                    severity="info"
                ></apex:pageMessage>

                <apex:form rendered="{!uiPage.uiMainPageMessage == null}">
                    <!-- Load Account -->
                    <apex:outputPanel layout="none" rendered="{!uiPage.stepperStep == uiStepperStep.loadAccountStep}">
                        <p>
                            <strong>Click "Load uPortal360 Account" to load the uPortal360 Account.</strong>
                        </p>

                        <apex:actionStatus id="loadAccountLoader">
                            <apex:facet name="start">
                                <img src="/img/loading.gif" />
                            </apex:facet>

                            <apex:facet name="stop">
                                <apex:commandButton
                                    value="Load uPortal360 Account"
                                    styleClass="lightning-button-primary lightning-font "
                                    action="{!onLoadAccount}"
                                    status="loadAccountLoader"
                                    reRender="mainBody"
                                />
                            </apex:facet>
                        </apex:actionStatus>
                    </apex:outputPanel>

                    <!-- Draft Credit Application -->
                    <apex:outputPanel rendered="{!uiPage.stepperStep == uiStepperStep.draftCreditApplication}" layout="none">
                        <p>
                            <strong
                                >Click "Draft Credit Application" to create a new credit application for this {!uiPage.standardObjectName}.</strong
                            >
                        </p>

                        <apex:actionStatus id="draftCreditApplicationLoader">
                            <apex:facet name="start">
                                <img src="/img/loading.gif" />
                            </apex:facet>

                            <apex:facet name="stop">
                                <apex:commandButton
                                    action="{!onDraftCreditApplication}"
                                    value="Draft Credit Application"
                                    styleClass="lightning-button-success lightning-font "
                                    status="draftCreditApplicationLoader"
                                    reRender="mainBody"
                                />
                            </apex:facet>
                        </apex:actionStatus>
                    </apex:outputPanel>

                    <!-- View Record Summary -->
                    <apex:outputPanel rendered="{!uiPage.stepperStep == uiStepperStep.viewRecordSummary}" layout="none">
                        <div style="margin-bottom: 16px">
                            <div style="display: flex; margin-bottom: 4px">
                                <apex:outputPanel rendered="{!uiPage.uiUASRecord != null}" layout="none">
                                    <div style="display: flex; align-items: center; width: 100%; gap: 4px; padding: 8px 0">
                                        <apex:actionStatus
                                            rendered="{!uiPage.uiUASRecord.isAllowSendApplicationEmail}"
                                            id="sendApplicationEmailLoader"
                                        >
                                            <apex:facet name="start">
                                                <img src="/img/loading.gif" />
                                            </apex:facet>

                                            <apex:facet name="stop">
                                                <apex:commandLink
                                                    action="{!onSendApplicationEmail}"
                                                    styleClass="btn lightning-icon-container"
                                                    style="text-decoration: none; padding: 4px"
                                                    status="sendApplicationEmailLoader"
                                                    reRender="mainBody"
                                                >
                                                    <img
                                                        class="lightning-icon"
                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAArRJREFUaEPtmNFR3DAYhD/DC2+5DkIHoYOkg6SDXAdQAnQAHUAH0MGlghwdHB1c3ngBZ9aWjGxsn2xJdshYMzfD3GF599/9/5Wd8cFX9sHxsxCYW8FFgUWBwAr81xb6DVwDd4FFSnp5nwK5ufPeIbJLimbE5j4E3G3vgRtgM+JeSS4ZSsCCkBKXwAMghWZbnQQyyK2HDNDvLSgF/h5OruB5Fnv5KqD/OwUuMljn8Elk9KVDUrZS00uVydYQAhbUCvhhLPS5hrRglO0gvzW9ktxeYwi4mL8Ba+Cn+6WjjCWyTSWJbw8cCjzZS0QuMPZqKLMlT5MpoQq0FdYS+dL8MYN9/haOUZo+BQGL+8woUrOXQypKpngQKDszwMNqellLyrQ0PWWmrHhgPzxTYvWALz+R0Odrd6ZwBQUpr+WrgDaT5Jr1v4DQqaKmV5JrHBeZ0ljemeJBwGxdOamILs133SSU0AqO1vAqi9XtVQblLofeTBlIoFNVl5CS2NsCzo7KFBGpjiz1mlVEaur3E2icFXpM+VSoccyGl0KVMQQEXP0hWxWrQUDPJTqqDCDQjbgErM8JG55HAdbumlAas6q8+qK5dB9ZSMBbjyW+FnriiA2vle/HVNgFp4w4NxWvgTaia1AItAZH7/IlEJIDLgBbbRFwjaK//xjAmk7eBZqCgKxhqy3LvLPJMVy+lNUefHpNSaC9Kd/aU9NKNgl6PI1N4FBTyiYCrcb0tklfE8Qi0NmU5uaPDvBDfTno91ACpimzs9rDZTXEi3dK72b3IIQJppCa0k6T1qY8NLvnIqCo1zSpkrIBxHt2T0Kg8VpFDdeWlKNm9yQE6m9MWiNegTNqds9JIMrsnppA9Nk9CYEMtuYNgkLnn12xDmmzEVwIzFZ65zA+N4ag+y8WCipfhIsXBSIUMWiLvzsZwjH/oLXeAAAAAElFTkSuQmCC"
                                                    />
                                                    <span class="lightning-font">&nbsp;Send application email</span>
                                                </apex:commandLink>
                                            </apex:facet>
                                        </apex:actionStatus>

                                        <apex:actionStatus id="loginAndViewInUportal360Loader">
                                            <apex:facet name="start">
                                                <img src="/img/loading.gif" />
                                            </apex:facet>

                                            <apex:facet name="stop">
                                                <apex:commandLink
                                                    action="{!onLoginAndViewUportal360}"
                                                    oncomplete="openOnNewTab('{!uiPage.loginAndViewInUportal360URL}')"
                                                    styleClass="btn lightning-icon-container"
                                                    style="text-decoration: none; padding: 4px"
                                                    status="loginAndViewInUportal360Loader"
                                                    reRender="mainBody"
                                                >
                                                    <img
                                                        class="lightning-icon"
                                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAAcRJREFUaEPtmetRwzAQhL+kgpQAHVACnQAVQAdAB3RAOqGGdEA6oIKYudjCGluyJEs5Y5B+ZeKTZvd2dXptWHnbrBw/lcDSClYF/oMCL8DzFFGxQeMO+ALegFdffw0LBQlEqCgEZJxR+50ExpJ8AtdLEYhIsDfEdpYz2RoKVAJdBqoCOVaY2/evzIFzWVKx0A54nFp4EqVQVUDAfwA3wB54SATrClclMFxxS5Bozhv+loaChbbsOXFnpTKXhKoCBreALkViEQJCpBSJjoBeFbInYk+i9fEcO2URcOzht8CpA2n/9tabJ2BnTcRUElkW8pwxunrg+jpxMjFlpEkrsWUIjHD9WHLAwv7fT+YA3AJy2gq1LAt1JyCfVaIsJKuyLHCmkB+aePDSJ0uBUHZC39+BeysoJfOmm/JC1qMtAd5SQLeMlgK/iIVKglcnIH4XAqbN8fxwXilP4n4zVwK8ugIme1J+5TYtps6HKpmyAiE46d8rgfScle1RFSibz/TRqgLpORv3cL4PBI4OZvdqLiSO0Fy5wGjcTvsfOGJYtKhX9sDRp/rYXRA4X2ckTEOBEjb0jlEJXDS9EYNXBSKSdNGQ1SvwDf+XdjF0io+BAAAAAElFTkSuQmCC"
                                                    />
                                                    <span class="lightning-font">&nbsp;Login and view in uPortal360</span>
                                                </apex:commandLink>
                                            </apex:facet>
                                        </apex:actionStatus>
                                    </div>
                                </apex:outputPanel>

                                <div style="display: flex; justify-content: flex-end; align-items: center; width: 100%; padding: 8px 0">
                                    <apex:actionStatus id="refreshRecordSummaryLoader">
                                        <apex:facet name="start">
                                            <img src="/img/loading.gif" />
                                        </apex:facet>

                                        <apex:facet name="stop">
                                            <apex:commandLink
                                                action="{!onRefreshRecordSummary}"
                                                styleClass="btn lightning-icon-container"
                                                style="text-decoration: none; padding: 4px"
                                                status="refreshRecordSummaryLoader"
                                                reRender="mainBody"
                                            >
                                                <img
                                                    class="lightning-icon"
                                                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAAXNSR0IArs4c6QAAAxBJREFUaEPtmO9xEzEQxX/ymM9xB4EKbCqIUwHpIFABoQJIBYQOoAJMBTYVYCpIqCDmMx4f887SodzYZ+lOSsbDacbjubm91Xv7T7syHPkyR46fnsBTe7D3QO+BjhboQ8gz4Ag4g8EFbJ4D7ieRJbAawN0GFsA3PTcYv/DeNRo5hQcE9D3wOiIaBH4GfGFLqL4ehYAsLuBXlMeJv2cEFbgBrmseyU5gBGYOxaQG9ae1rKx6Z38SmVo5/ctTp/53BpYFvLGhpldZCUwwzCmQB9z6vvVEGeshS0Qk/8oTVlidWx3ZCAj0LVTgf1sgn0NQ75CRNxRCJ/adI/FDzzYwkyWxwM8BFzYCL0uGWn0fx4mBWfEvrETC924yArLUW4siFXhHSpVMhth64mFNSEJAG9x6mpVwbcNmtyeGTFkz31HPkhAQ2EtLQAnrqkps6Cv8XLzv+lY5UT9POhMYGbj3ysLLDnGv8noWybozgQvgq91Udb5e+2PwPAkBGz4lTp2YH2IQ+7IDuNlYA0Sc3Y3hGtIL+VbTQbOrd2nLqfN3IQTUErij/4XXHnTePIWCEALBx3oKQLE6/gsCRx9CR5/EycpobHyHyIfkgH+QqeHSSZxzqWkc2w0+2QFp734hBNTa3nsaMp4Fwyms5143erBshxAQ9iqMDCyK7eSUY/n5ppsLeb9xhRKw7XSlK1M7Xc7ZbpMgT4cSkFJ/oFnBs3P403Uac2DVIGraU+crCrpuCbqmiSGgXJCLXYL5Q/ghTze9r8BbIU178njTxVelL4aAPhIJHWzlUGJgVcC71tPZgCs2fPTYRY+qsQS0lyy2AHPixetiCNfr8E5V4aFLMVnaLYFX0kZ1u20IOE8sDIxr93HKCV0ZCsQv660RQ8asq7tSgfeBS1+05R3rtgQcCQ037qbiYB7sGWJ0WElPUMzXN+lCwOmSNQXg8iCDhwKqNKpsnSpZCgIOlhJc45/iWKT07CqWQmQ5gOWmBDyawaqVxXN4INLwacVTeiAtskBtPYFAQ2UT6z2QzbSBinsPBBoqm9hfGJelMS/0pwsAAAAASUVORK5CYII="
                                                />
                                                <span class="lightning-font">&nbsp;Refresh</span>
                                            </apex:commandLink>
                                        </apex:facet>
                                    </apex:actionStatus>
                                </div>
                            </div>

                            <apex:pageMessage
                                rendered="{!uiPage.uiViewRecordSummaryPageMessage != null}"
                                strength="{!uiPage.uiViewRecordSummaryPageMessage.strength}"
                                title="{!uiPage.uiViewRecordSummaryPageMessage.title}"
                                summary="{!uiPage.uiViewRecordSummaryPageMessage.message}"
                                severity="{!uiPage.uiViewRecordSummaryPageMessage.severity}"
                            ></apex:pageMessage>
                        </div>

                        <apex:outputPanel rendered="{!uiPage.uiUASRecord == null}" layout="none">
                            <p><strong>No data available</strong></p>
                        </apex:outputPanel>

                        <apex:outputPanel rendered="{!uiPage.uiUASRecord != null}" layout="none">
                            <apex:outputPanel rendered="{!uiPage.uiUASRecord.contract != null}" layout="none">
                                <table class="lightning-table">
                                    <thead>
                                        <tr>
                                            <th>Contract Detail</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td><strong>Contract Number:</strong> {!uiPage.uiUASRecord.contract.contractNumber}</td>
                                            <td>
                                                <strong>Status:</strong>
                                                <span style="background-color: #ffff00">{!uiPage.uiUASRecord.contract.status}</span>
                                            </td>
                                            <td>
                                                <strong>Substatus:</strong>
                                                <span style="background-color: #ffff00">{!uiPage.uiUASRecord.contract.substatus}</span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><strong>Note Date:</strong> {!uiPage.uiUASRecord.contract.noteDate}</td>
                                            <td><strong>First Payment Date:</strong> {!uiPage.uiUASRecord.contract.firstPaymentDate}</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td><strong>Credit Used:</strong> {!uiPage.uiUASRecord.contract.creditUsed}</td>
                                            <td><strong>Credit Limit:</strong> {!uiPage.uiUASRecord.contract.creditLimit}</td>
                                            <td><strong>Credit Remaining:</strong> {!uiPage.uiUASRecord.contract.creditRemaining}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </apex:outputPanel>

                            <apex:outputPanel rendered="{!uiPage.uiUASRecord.creditApplicationId != null}" layout="none">
                                <table class="lightning-table">
                                    <thead>
                                        <tr>
                                            <th>Application Detail</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td>
                                                <strong>Status:</strong> <span style="background-color: #ffff00">{!uiPage.uiUASRecord.status}</span>
                                            </td>
                                            <td><strong>Credit Application Id:</strong> {!uiPage.uiUASRecord.creditApplicationId}</td>
                                            <td><strong>Contract Number:</strong> {!uiPage.uiUASRecord.contractNumber}</td>
                                        </tr>

                                        <tr>
                                            <td><strong>Location:</strong> {!uiPage.uiUASRecord.location}</td>
                                            <td><strong>Program:</strong> {!uiPage.uiUASRecord.program}</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </apex:outputPanel>

                            <apex:outputPanel rendered="{!uiPage.uiUASRecord.applicant != null}" layout="none">
                                <table class="lightning-table">
                                    <thead>
                                        <tr>
                                            <th>Applicant Detail</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td><strong>First Name:</strong> {!uiPage.uiUASRecord.applicant.firstName}</td>
                                            <td><strong>Middle Initial:</strong> {!uiPage.uiUASRecord.applicant.middleInitial}</td>
                                            <td><strong>Last Name:</strong> {!uiPage.uiUASRecord.applicant.lastName}</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td><strong>Email:</strong> {!uiPage.uiUASRecord.applicant.email}</td>
                                            <td><strong>DOB:</strong> {!uiPage.uiUASRecord.applicant.dob}</td>
                                            <td><strong>Last 4 SSN:</strong> {!uiPage.uiUASRecord.applicant.ssnLast4}</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td><strong>Primary Type:</strong> {!uiPage.uiUASRecord.applicant.primaryNumberType}</td>
                                            <td><strong>Primary Phone:</strong> {!uiPage.uiUASRecord.applicant.primaryNumber}</td>
                                            <td><strong>Secondary Type:</strong> {!uiPage.uiUASRecord.applicant.secondaryNumberType}</td>
                                            <td><strong>Secondary Phone:</strong> {!uiPage.uiUASRecord.applicant.secondaryNumber}</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td><strong>Annual Income:</strong> {!uiPage.uiUASRecord.applicant.annualIncome}</td>
                                            <td><strong>Residence Type:</strong> {!uiPage.uiUASRecord.applicant.residenceType}</td>
                                            <td><strong>Monthly Expense:</strong> {!uiPage.uiUASRecord.applicant.residenceMonthlyExpense}</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td><strong>Address Line 1:</strong> {!uiPage.uiUASRecord.applicant.line1}</td>
                                            <td><strong>Address Line 2:</strong> {!uiPage.uiUASRecord.applicant.line2}</td>
                                            <td><strong>City:</strong> {!uiPage.uiUASRecord.applicant.city}</td>
                                            <td><strong>State:</strong> {!uiPage.uiUASRecord.applicant.state}</td>
                                            <td><strong>Zip:</strong> {!uiPage.uiUASRecord.applicant.zip}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </apex:outputPanel>

                            <apex:outputPanel rendered="{!uiPage.uiUASRecord.coapplicant != null}" layout="none">
                                <table class="lightning-table">
                                    <thead>
                                        <tr>
                                            <th>Co-Applicant Detail</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td><strong>First Name:</strong> {!uiPage.uiUASRecord.coapplicant.firstName}</td>
                                            <td><strong>Middle Initial:</strong> {!uiPage.uiUASRecord.coapplicant.middleInitial}</td>
                                            <td><strong>Last Name:</strong> {!uiPage.uiUASRecord.coapplicant.lastName}</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td><strong>Email:</strong> {!uiPage.uiUASRecord.coapplicant.email}</td>
                                            <td><strong>DOB:</strong> {!uiPage.uiUASRecord.coapplicant.dob}</td>
                                            <td><strong>Last 4 SSN:</strong> {!uiPage.uiUASRecord.coapplicant.ssnLast4}</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td><strong>Primary Type:</strong> {!uiPage.uiUASRecord.coapplicant.primaryNumberType}</td>
                                            <td><strong>Primary Phone:</strong> {!uiPage.uiUASRecord.coapplicant.primaryNumber}</td>
                                            <td><strong>Secondary Type:</strong> {!uiPage.uiUASRecord.coapplicant.secondaryNumberType}</td>
                                            <td><strong>Secondary Phone:</strong> {!uiPage.uiUASRecord.coapplicant.secondaryNumber}</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td><strong>Annual Income:</strong> {!uiPage.uiUASRecord.coapplicant.annualIncome}</td>
                                            <td><strong>Residence Type:</strong> {!uiPage.uiUASRecord.coapplicant.residenceType}</td>
                                            <td><strong>Monthly Expense:</strong> {!uiPage.uiUASRecord.coapplicant.residenceMonthlyExpense}</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td><strong>Address Line 1:</strong> {!uiPage.uiUASRecord.coapplicant.line1}</td>
                                            <td><strong>Address Line 2:</strong> {!uiPage.uiUASRecord.coapplicant.line2}</td>
                                            <td><strong>City:</strong> {!uiPage.uiUASRecord.coapplicant.city}</td>
                                            <td><strong>State:</strong> {!uiPage.uiUASRecord.coapplicant.state}</td>
                                            <td><strong>Zip:</strong> {!uiPage.uiUASRecord.coapplicant.zip}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </apex:outputPanel>
                        </apex:outputPanel>
                    </apex:outputPanel>
                </apex:form>
            </apex:outputPanel>
        </body>
    </html>
</apex:page>
