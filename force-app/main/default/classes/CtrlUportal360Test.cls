@isTest
public without sharing class CtrlUportal360Test {
    //#region Constants
    private final static String CONTACT_SOBJECT_NAME = 'Contact';
    private final static String LEAD_SOBJECT_NAME = 'Lead';
    private final static String LOREM_IPSUM = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit';
    //#endregion

    //#region Setup
    @testSetup
    static void setup() {
        Database.insert(new UAS_uPortal360_Settings__c(Name = 'Main', UAS_Program_Number__c = 'ABC99', UAS_Location_Number__c = 'ABC1234'), true);
    }
    //#endregion

    //#region HTTP Mock
    class TemporaryLoginLinkResponse implements HttpCalloutMock {
        public HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            res.setBody('{"data":{"url":"https://staging.uportal360.com/login/temporary/xyz"}}');
            return res;
        }
    }

    class DraftApplicationMockResponse implements HttpCalloutMock {
        public HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);

            if (req.getEndpoint().containsIgnoreCase('applications')) {
                if (req.getMethod().equalsIgnoreCase('POST')) {
                    res.setBody(
                        '{"data":{"id":"84b865d2f2634a46b0f1","accountNumber":"65AB7DC","status":"DRAFT","applyUrl":"https://test-apply.uportal360.com/?id=84b865d2f2634a46b0f1","edit":{"href":"https://test-api.uportal360.com/applications/84b865d2f2634a46b0f1","method":"PATCH"},"self":{"href":"https://test-api.uportal360.com/applications/84b865d2f2634a46b0f1","method":"GET"}}}'
                    );
                } else if ((req.getMethod().equalsIgnoreCase('GET'))) {
                    res.setBody(
                        '{"data":{"application":{"contract":{"cashPrice":null,"downPayment":null,"description":null,"email":null,"id":null},"applicant":{"firstName":"Approved","middleInitial":null,"lastName":"Testcase","email":"test@ugafinance.com","dob":"2000-11-01","ssnLast4":"1001","primaryNumber":"1111111111","primaryNumberType":"Mobile","secondaryNumber":"2222222222","secondaryNumberType":"Home","annualIncome":60000.00,"residenceMonthlyExpense":1.0,"residenceType":"Own","employmentType":null,"address":{"line1":"220 Locust Ave","line2":null,"city":"Anthill","state":"MO","zip":"65488"}},"coApplicant":{"firstName":null,"middleInitial":null,"lastName":null,"email":null,"dob":null,"ssnLast4":null,"primaryNumber":null,"primaryNumberType":null,"secondaryNumber":null,"secondaryNumberType":null,"annualIncome":null,"residenceMonthlyExpense":null,"residenceType":null,"employmentType":null,"address":{"line1":null,"line2":null,"city":null,"state":null,"zip":null}},"clientId":null,"location":"ABC1234","program":"ABC99","status":"Approved","pendingReason":null,"id":"84b865d2f2634a46b0f1"}}}'
                    );
                } else {
                    System.assert(false, 'Unknown mock endpoint method.');
                }
            } else if (req.getEndpoint().containsIgnoreCase('generateTemporaryLoginLink')) {
                res.setBody('{"data":{"url":"https://staging.uportal360.com/login/temporary/xyz"}}');
            } else {
                System.assert(false, 'Unknown mock endpoint.');
            }

            return res;
        }
    }

    class GetProcessedRecordMockResponse implements HttpCalloutMock {
        public HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);

            if (req.getEndpoint().containsIgnoreCase('applications')) {
                res.setBody(
                    '{"data":{"application":{"contract":{"cashPrice":null,"downPayment":null,"description":null,"email":"test@ugafinance.com","id":"65AB7DCLOA01"},"applicant":{"firstName":"Approved","middleInitial":null,"lastName":"Testcase","email":"test@ugafinance.com","dob":"2000-11-01","ssnLast4":"1001","primaryNumber":"1111111111","primaryNumberType":"Mobile","secondaryNumber":"2222222222","secondaryNumberType":"Home","annualIncome":60000.00,"residenceMonthlyExpense":1.0,"residenceType":"Own","employmentType":null,"address":{"line1":"220 Locust Ave","line2":null,"city":"Anthill","state":"MO","zip":"65488"}},"coApplicant":{"firstName":null,"middleInitial":null,"lastName":null,"email":null,"dob":null,"ssnLast4":null,"primaryNumber":null,"primaryNumberType":null,"secondaryNumber":null,"secondaryNumberType":null,"annualIncome":null,"residenceMonthlyExpense":null,"residenceType":null,"employmentType":null,"address":{"line1":null,"line2":null,"city":null,"state":null,"zip":null}},"clientId":null,"location":"ABC1234","program":"ABC99","status":"Approved","pendingReason":null,"id":"84b865d2f2634a46b0f1"}}}'
                );
            } else if (req.getEndpoint().containsIgnoreCase('contracts')) {
                res.setBody(
                    '{"data":{"id":"65AB7DCLOA01","applicant":{"firstName":"Approved","lastName":"Testcase"},"status":"Pending - Consumer","substatus":"Payment Option","noteDate":"2022-11-03","firstPaymentDate":"2022-12-13","delinquentDays":0,"credit":{"used":0.0,"limit":10000.0,"remaining":10000.0},"balance":{"principal":0.0,"interest":0.0,"fees":0.0,"total":0.0},"relatedCases":[]}}'
                );
            } else if (req.getEndpoint().containsIgnoreCase('generateTemporaryLoginLink')) {
                res.setBody('{"data":{"url":"https://staging.uportal360.com/login/temporary/xyz"}}');
            } else {
                System.assert(false, 'Unknown mock endpoint.');
            }

            return res;
        }
    }

    class GetLegacyRecordMockResponse implements HttpCalloutMock {
        public HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);

            if (req.getEndpoint().containsIgnoreCase('contracts')) {
                res.setBody(
                    '{"data":{"id":"65AB7DCLOA01","applicant":{"firstName":"Approved","lastName":"Testcase"},"status":"Pending - Consumer","substatus":"Payment Option","noteDate":"2022-11-03","firstPaymentDate":"2022-12-13","delinquentDays":0,"credit":{"used":0.0,"limit":10000.0,"remaining":10000.0},"balance":{"principal":0.0,"interest":0.0,"fees":0.0,"total":0.0},"relatedCases":[]}}'
                );
            } else if (req.getEndpoint().containsIgnoreCase('generateTemporaryLoginLink')) {
                res.setBody('{"data":{"url":"https://staging.uportal360.com/login/temporary/xyz"}}');
            } else {
                System.assert(false, 'Unknown mock endpoint.');
            }

            return res;
        }
    }

    class SendApplicationLinkMockResponse implements HttpCalloutMock {
        public HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(204);
            return res;
        }
    }

    class UnAuthorizedMockResponse implements HttpCalloutMock {
        public HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(401);
            return res;
        }
    }

    class FailedMockResponse implements HttpCalloutMock {
        public HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"errors":[{"message":"Error message"}]}');
            res.setStatusCode(500);
            return res;
        }
    }
    //#endregion

    //#region Tests
    @isTest
    static void getAPIErrorMessage() {
        Test.startTest();
        final CtrlUportal360 classToTest = new CtrlUportal360();
        System.assert(String.isEmpty(classToTest.getAPIErrorMessage('{"data":{"id":"RANDOM_STRING"}}')), 'Expected to be true when no error.');
        System.assert(
            String.isNotEmpty(classToTest.getAPIErrorMessage('{"errors":[{"message":"RANDOM_STRING"}]}')),
            'Expected to be false when there is an error.'
        );
        Test.stopTest();
    }

    @isTest
    static void callout() {
        Test.setMock(HttpCalloutMock.class, new FailedMockResponse());

        Test.startTest();
        Boolean noException = true;
        try {
            final CtrlUportal360 classToTest = new CtrlUportal360();
            classToTest.callout('GET', '', null);
        } catch (Exception ignored) {
            noException = false;
        }
        Test.stopTest();
        System.assert(noException, 'Exception should not have been occurred.');
    }

    @isTest
    static void setRecordReferences() {
        Database.SaveResult contactMock = Database.insert(
            new Contact(LastName = 'TESTCASE', MobilePhone = '1112223333', HomePhone = '4445556666'),
            true
        );
        Database.SaveResult leadMock = Database.insert(
            new Lead(LastName = 'TESTCASE', Company = 'TESTER_CO', MobilePhone = '1112223333', Phone = '4445556666'),
            true
        );

        Test.startTest();
        Boolean noException = true;
        try {
            final CtrlUportal360 classToTest = new CtrlUportal360();
            classToTest.setRecordReferences(contactMock.getId(), CONTACT_SOBJECT_NAME);
            classToTest.setRecordReferences(leadMock.getId(), LEAD_SOBJECT_NAME);
        } catch (Exception ignored) {
            noException = false;
        }
        Test.stopTest();
        System.assert(noException, 'Exception should not have been occurred.');
    }

    @isTest
    static void updateStandardObjectRecord() {
        Database.SaveResult contactMock = Database.insert(new Contact(LastName = 'TESTCASE', HomePhone = '1112223333', Phone = '4445556666'), true);
        Database.SaveResult leadMock = Database.insert(new Lead(LastName = 'TESTCASE', Company = 'TESTER_CO'), true);

        Test.startTest();
        Boolean noException = true;
        try {
            final CtrlUportal360 classToTest = new CtrlUportal360();

            CtrlUportal360.StandardObjectRecord contactRecord = new CtrlUportal360.StandardObjectRecord();
            contactRecord.objectId = contactMock.getId();
            contactRecord.objectName = CONTACT_SOBJECT_NAME;
            classToTest.updateStandardObjectRecord(contactRecord);

            CtrlUportal360.StandardObjectRecord leadRecord = new CtrlUportal360.StandardObjectRecord();
            leadRecord.objectId = leadMock.getId();
            leadRecord.objectName = LEAD_SOBJECT_NAME;
            classToTest.updateStandardObjectRecord(leadRecord);
        } catch (Exception ignored) {
            noException = false;
        }
        Test.stopTest();
        System.assert(noException, 'Exception should not have been occurred.');
    }

    @isTest
    static void newErrorPageMessage() {
        Test.startTest();
        final CtrlUportal360 classToTest = new CtrlUportal360();

        CtrlUportal360.UIPageMessage result = classToTest.newErrorPageMessage(0, LOREM_IPSUM);
        System.assert(LOREM_IPSUM.equals(result.message), 'Expected to be equal.');
        System.assert('error'.equals(result.severity), 'Expected to be equal.');
        System.assert(result.strength == 0, 'Expected to be equal.');
        Test.stopTest();
    }

    @isTest
    static void newInfoPageMessage() {
        Test.startTest();
        final CtrlUportal360 classToTest = new CtrlUportal360();

        CtrlUportal360.UIPageMessage result = classToTest.newInfoPageMessage(0, LOREM_IPSUM);
        System.assert(LOREM_IPSUM.equals(result.message), 'Expected to be equal.');
        System.assert('info'.equals(result.severity), 'Expected to be equal.');
        System.assert(result.strength == 0, 'Expected to be equal.');
        Test.stopTest();
    }

    @isTest
    static void loadUIUASRecordByCreditApplicationId() {
        Test.setMock(HttpCalloutMock.class, new GetProcessedRecordMockResponse());

        Test.startTest();
        final CtrlUportal360 classToTest = new CtrlUportal360();
        System.assert(classToTest.loadUIUASRecordByCreditApplicationId('randomId'), 'Expected to be true.');
        Test.stopTest();
    }

    @isTest
    static void loadUIUASRecordByContractNumber() {
        Test.setMock(HttpCalloutMock.class, new GetLegacyRecordMockResponse());

        Test.startTest();
        final CtrlUportal360 classToTest = new CtrlUportal360();
        System.assert(classToTest.loadUIUASRecordByContractNumber('randomId'), 'Expected to be true.');
        Test.stopTest();
    }

    @isTest
    static void checkIfValidAPIResponse() {
        Test.startTest();
        final CtrlUportal360 classToTest = new CtrlUportal360();

        Boolean expectException = false;
        try {
            Test.setMock(HttpCalloutMock.class, new UnAuthorizedMockResponse());
            HttpResponse res = classToTest.callout('GET', '', null);
            classToTest.checkIfValidAPIResponse(res);
        } catch (Exception ignored) {
            expectException = true;
        }
        System.assert(expectException, 'Exception should have occur.');

        expectException = false;
        try {
            Test.setMock(HttpCalloutMock.class, new FailedMockResponse());
            HttpResponse res = classToTest.callout('GET', '', null);
            classToTest.checkIfValidAPIResponse(res);
        } catch (Exception ignored) {
            expectException = true;
        }
        System.assert(expectException, 'Exception should have occur.');
        Test.stopTest();
    }

    @isTest
    static void getStandardObjectId() {
        Test.setCurrentPage(Page.VFUportal360Contact);

        Database.SaveResult contactMock = Database.insert(new Contact(LastName = 'TESTCASE', OtherPhone = '1112223333'), true);
        ApexPages.currentPage().getParameters().put('id', contactMock.getId());

        Test.startTest();
        final CtrlUportal360 classToTest = new CtrlUportal360();
        System.assert(classToTest.getStandardObjectId() != null, 'Expected to be not null.');
        Test.stopTest();
    }

    @isTest
    static void getStandardObjectName() {
        Test.setCurrentPage(Page.VFUportal360Contact);

        Database.SaveResult contactMock = Database.insert(new Contact(LastName = 'TESTCASE', MobilePhone = '1112223333', Phone = '4445556666'), true);
        ApexPages.currentPage().getParameters().put('id', contactMock.getId());

        Test.startTest();
        final CtrlUportal360 classToTest = new CtrlUportal360();
        System.assert(String.isNotEmpty(classToTest.getStandardObjectName()), 'Expected to be not empty.');
        Test.stopTest();
    }

    @isTest
    static void getSettings() {
        Test.startTest();
        final CtrlUportal360 classToTest = new CtrlUportal360();
        System.assert(classToTest.getSettings() != null, 'Expected to be not null.');
        Test.stopTest();
    }

    @isTest
    static void getTemporaryLoginLink() {
        Test.startTest();
        final CtrlUportal360 classToTest = new CtrlUportal360();

        Boolean expectException = false;
        try {
            Test.setMock(HttpCalloutMock.class, new FailedMockResponse());
            classToTest.getTemporaryLoginLink();
        } catch (Exception ignored) {
            expectException = true;
        }
        System.assert(expectException, 'Exception should have occur.');

        Test.setMock(HttpCalloutMock.class, new TemporaryLoginLinkResponse());
        System.assert(String.isNotEmpty(classToTest.getTemporaryLoginLink()), 'Expected to be not empty.');

        Test.stopTest();
    }

    @isTest
    static void sendApplicationLink() {
        Test.startTest();
        final CtrlUportal360 classToTest = new CtrlUportal360();

        Test.setMock(HttpCalloutMock.class, new GetProcessedRecordMockResponse());
        System.assert(classToTest.loadUIUASRecordByCreditApplicationId('randomId'), 'Expected to be true.');

        Test.setMock(HttpCalloutMock.class, new SendApplicationLinkMockResponse());
        System.assert(
            classToTest.sendApplicationLink(new CtrlUportal360.PrefilledAppLinkRequest('84b865d2f2634a46b0f1', 'test@ugafinance.com')),
            'Expected to be true.'
        );
        System.assert(
            !classToTest.sendApplicationLink(new CtrlUportal360.PrefilledAppLinkRequest('84b865d2f2634a46b0f1', '')),
            'Expected to be false.'
        );
        Test.stopTest();
    }

    @isTest
    static void naIfNullString() {
        Test.startTest();
        System.assert(LOREM_IPSUM.equals(CtrlUportal360.naIfNull(LOREM_IPSUM)), 'Expected to be equal.');
        System.assert('N/A'.equals(CtrlUportal360.naIfNull((String) null)), 'Expected to be equal.');
        System.assert('N/A'.equals(CtrlUportal360.naIfNull('')), 'Expected to be equal.');
        Test.stopTest();
    }

    @isTest
    static void naIfNullDecimal() {
        Test.startTest();
        System.assert('N/A'.equals(CtrlUportal360.naIfNull((Decimal) null)), 'Expected to be equal.');
        System.assert('123'.equals(CtrlUportal360.naIfNull(123)), 'Expected to be equal.');
        Test.stopTest();
    }

    @isTest
    static void getISODate() {
        Test.startTest();
        System.assert('1960-02-17'.equals(CtrlUportal360.getISODate(Date.newInstance(1960, 2, 17))), 'Expected to be equal.');
        Test.stopTest();
    }

    @isTest
    static void getE164PhoneNumber() {
        Test.startTest();
        System.assert(String.isEmpty(CtrlUportal360.getE164PhoneNumber('')), 'Expected to be equal.');
        System.assert('+11112223333'.equals(CtrlUportal360.getE164PhoneNumber('1112223333')), 'Expected to be equal.');
        System.assert('+11112223333'.equals(CtrlUportal360.getE164PhoneNumber('(111)2223333')), 'Expected to be equal.');
        System.assert('+11112223333'.equals(CtrlUportal360.getE164PhoneNumber('(111) 222-3333')), 'Expected to be equal.');
        Test.stopTest();
    }

    @isTest
    static void init() {
        Test.setCurrentPage(Page.VFUportal360Contact);

        Database.SaveResult contactMock = Database.insert(new Contact(LastName = 'TESTCASE'), true);
        ApexPages.currentPage().getParameters().put('id', contactMock.getId());

        Test.startTest();
        Boolean noException = true;
        try {
            new CtrlUportal360();
        } catch (Exception ignored) {
            noException = false;
        }
        Test.stopTest();
        System.assert(noException, 'Exception should not have been occurred.');
    }

    @isTest
    static void onLoadAccount() {
        Test.setCurrentPage(Page.VFUportal360Contact);

        Database.SaveResult contactMock = Database.insert(new Contact(LastName = 'TESTCASE'), true);
        ApexPages.currentPage().getParameters().put('id', contactMock.getId());

        Test.startTest();
        Boolean noException = true;
        try {
            final CtrlUportal360 classToTest = new CtrlUportal360();
            classToTest.onLoadAccount();
        } catch (Exception ignored) {
            noException = false;
        }
        Test.stopTest();
        System.assert(noException, 'Exception should not have been occurred.');
    }

    @isTest
    static void onDraftCreditApplication() {
        Test.setCurrentPage(Page.VFUportal360Contact);
        Test.setMock(HttpCalloutMock.class, new DraftApplicationMockResponse());

        Database.SaveResult contactMock = Database.insert(new Contact(LastName = 'TESTCASE'), true);
        ApexPages.currentPage().getParameters().put('id', contactMock.getId());

        Test.startTest();
        Boolean noException = true;
        try {
            final CtrlUportal360 classToTest = new CtrlUportal360();
            classToTest.onLoadAccount();
            classToTest.onDraftCreditApplication();
        } catch (Exception ignored) {
            noException = false;
        }
        Test.stopTest();
        System.assert(noException, 'Exception should not have been occurred.');
    }

    @isTest
    static void onRefreshRecordSummaryA() {
        Test.setCurrentPage(Page.VFUportal360Contact);
        Test.setMock(HttpCalloutMock.class, new GetProcessedRecordMockResponse());

        Database.SaveResult contactMock = Database.insert(
            new Contact(LastName = 'TESTCASE', UAS_Credit_Application_Id__c = '84b865d2f2634a46b0f1'),
            true
        );
        ApexPages.currentPage().getParameters().put('id', contactMock.getId());

        Test.startTest();
        Boolean noException = true;
        try {
            final CtrlUportal360 classToTest = new CtrlUportal360();
            classToTest.onLoadAccount();
            classToTest.onRefreshRecordSummary();
        } catch (Exception ignored) {
            noException = false;
        }
        Test.stopTest();
        System.assert(noException, 'Exception should not have been occurred.');
    }

    @isTest
    static void onRefreshRecordSummaryB() {
        Test.setCurrentPage(Page.VFUportal360Contact);
        Test.setMock(HttpCalloutMock.class, new GetLegacyRecordMockResponse());

        Database.SaveResult contactMock = Database.insert(new Contact(LastName = 'TESTCASE', UAS_Contract_Number__c = '65AB7DCLOA01'), true);
        ApexPages.currentPage().getParameters().put('id', contactMock.getId());

        Test.startTest();
        Boolean noException = true;
        try {
            final CtrlUportal360 classToTest = new CtrlUportal360();
            classToTest.onLoadAccount();
            classToTest.onRefreshRecordSummary();
        } catch (Exception ignored) {
            noException = false;
        }
        Test.stopTest();
        System.assert(noException, 'Exception should not have been occurred.');
    }

    @isTest
    static void onSendApplicationEmail() {
        Test.setCurrentPage(Page.VFUportal360Contact);

        Database.SaveResult contactMock = Database.insert(
            new Contact(LastName = 'TESTCASE', UAS_Credit_Application_Id__c = '84b865d2f2634a46b0f1'),
            true
        );
        ApexPages.currentPage().getParameters().put('id', contactMock.getId());

        Test.startTest();
        Boolean noException = true;
        try {
            Test.setMock(HttpCalloutMock.class, new GetProcessedRecordMockResponse());
            final CtrlUportal360 classToTest = new CtrlUportal360();
            classToTest.onLoadAccount();

            Test.setMock(HttpCalloutMock.class, new SendApplicationLinkMockResponse());
            classToTest.onSendApplicationEmail();
        } catch (Exception ignored) {
            noException = false;
        }
        Test.stopTest();
        System.assert(noException, 'Exception should not have been occurred.');
    }

    @isTest
    static void onLoginAndViewUportal360() {
        Test.setCurrentPage(Page.VFUportal360Contact);

        Database.SaveResult contactMock = Database.insert(
            new Contact(LastName = 'TESTCASE', UAS_Credit_Application_Id__c = '84b865d2f2634a46b0f1'),
            true
        );
        ApexPages.currentPage().getParameters().put('id', contactMock.getId());

        Test.startTest();
        Boolean noException = true;
        try {
            Test.setMock(HttpCalloutMock.class, new GetProcessedRecordMockResponse());
            final CtrlUportal360 classToTest = new CtrlUportal360();
            classToTest.onLoadAccount();
            classToTest.onLoginAndViewUportal360();
        } catch (Exception ignored) {
            noException = false;
        }
        Test.stopTest();
        System.assert(noException, 'Exception should not have been occurred.');
    }
    //#endregion
}
